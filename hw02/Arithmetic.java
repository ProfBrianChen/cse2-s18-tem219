//Thomas Macon
//CSE2 hw02
//2.5.18

public class Arithmetic {
  public static void main(String[] args) {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numshirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //Cost per belt
    double beltPrice = 33.99;
    
    //tax rate
    double paSalesTax = 0.06;
    
    //calculated cost of each kind of item before tax
    double totCostPants = numPants * pantsPrice;
    double totCostShirts = numshirts * shirtPrice;
    double totCostBelts = numBelts * beltPrice;
    
    //calculated cost of tax on each kind of item
    double taxPants = paSalesTax * totCostPants;
    double taxShirts = paSalesTax * totCostShirts;
    double taxBelts = paSalesTax * totCostBelts;
    
    //cost of all purchases before tax
    double totCostBeforeTax = totCostPants + totCostShirts + totCostBelts;
    
    //total sales tax
    double totTax = taxPants + taxShirts + taxBelts;
    
    //cost of all purchases after taxPants
    double totCostAfterTax = totCostBeforeTax + totTax;
    
    //DecimalFormat numberFormat = new DecimalFormat("#.00");
    
    System.out.printf("Total cost before sales tax = $%#.2f\n",totCostBeforeTax);
    System.out.printf("Total sales tax = $%#.2f\n", totTax);
    System.out.printf("Total cost after sales tax = $%#.2f\n", totCostAfterTax);
    
  }
}

