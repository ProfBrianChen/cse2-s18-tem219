//Thomas Macon
//CSE2 hw10
//4.24.18
//Professor Brian Chen

//This program calls a method to create a two-dimensional array where each value represents a city block. Another method is used 
//to "invade" the city with a specified number of "robots" that are represented by changing the city block value to a negative.
//Then another method is called that shifts all the robots one block to the east (if a robot was on the easternmost block then
//it disappears). Finally, a different method is used to display these three iterations of the city with formatting so that even with
//negative numbers it maintains a clean, even grid. This entire process of creating a city, invading a city, then shifting the robots
//is completed five times with a for loop in the main method.

import java.util.Random;

public class RobotCity {
  public static void main(String[] args) {
    Random randGenerator = new Random();
    for (int i = 1; i <= 5; i++) { //iterates through this block of code five times
      int[][] cityArray = buildCity(); //calls method that creates city 
      display(cityArray); //calls method to print out initial city
      System.out.println();
      
      int numRobots = randGenerator.nextInt(cityArray.length * cityArray[0].length); //generates a random number that will be how many robots invade the city
      int[][] invadedCity = invade(cityArray, numRobots); //calls method to put a random amount of robots in the city
      display(invadedCity); //calls method to print out invaded city
      System.out.println();
      
      int[][] updatedCity = update(invadedCity); //calls method to shift robots to the east by one block
      display(updatedCity); //calls method to print out updated invaded city
      System.out.println();
    }
  } //END OF MAIN METHOD
  
  //This method creates a city
  public static int[][] buildCity() {
    Random randGenerator = new Random(); 
    int width = randGenerator.nextInt(6) + 10; //generates random number between 10 and 15, inclusive
    int height = randGenerator.nextInt(6) + 10; //generates random number between 10 and 15, inclusive
    
    int[][] cityArray = new int[height][width]; //initializes city with random heigh and width
    
    for (int i = 0; i < height; i++) { //iterates through height of city
      for (int j = 0; j < width; j++) { //iterates through width of city
        int rand = randGenerator.nextInt(900) + 100; //generates random number between 100 and 999, inclusive
        cityArray[i][j] = rand; //assigns random value to city block
      }
    }   
    return cityArray;
  } //END OF buildCity
  
  //This method prints out an input city
  public static void display(int[][] cityArray) {
    for (int i = 0; i < cityArray.length; i++) { //iterates through height of city
      for (int j = 0; j < cityArray[0].length; j++) { //iterates through width of city
        System.out.printf("%4d ", cityArray[i][j]); //prints out value at current city block
      }
      System.out.println();
    }
  } //END OF display
  
  //This method puts "robots" in the city that are represented by negative signs
  public static int[][] invade(int[][] cityArray, int k) {
    Random randGenerator = new Random();
    for (int i = 0; i < k; i++) { //iterates through however many robots (input variable k) there will be in the city
      int x = randGenerator.nextInt(cityArray[0].length); //generates a random x-coordinate
      int y = randGenerator.nextInt(cityArray.length); //generates a random y-coordinate
      if (cityArray[y][x] > 0) { //checks that there isn't already a robot at this location
        cityArray[y][x] *= -1; //if there isn't a robot at this location, this adds one by setting the city block's value to negative
      } else { //if there is already a  robot at this location, the method deincrements the counter so that at the end of the loop
        //there are still k number of robots
        i--;
      }
    }
    return cityArray;
  } //END OF invade
  
  //This method shifts every robot's location one block to the east 
  public static int[][] update(int[][] cityArray) {
    for (int i = 0; i < cityArray.length; i++) { //iterates over height of city
      for (int j = cityArray[0].length-1; j >= 0; j--) { //iterates over width of city from right to left
        if (cityArray[i][j] < 0) { //checks if there is a robot at this location
          cityArray[i][j] *= -1; //if there is, it sets the value back to positive to indicate the robot has moved on
          if (j < cityArray[0].length-1) { //checks if there is space in the city for the robot to move to the east
            cityArray[i][j+1] *= -1; //if there is, moves robot one block east 
          }
        }
      }
    }
    return cityArray;
  } //END OF update
  
} //END OF MAIN METHOD