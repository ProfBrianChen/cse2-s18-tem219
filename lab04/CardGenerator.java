//Thomas Macon
//CSE2 lab04
//Brian Chen
//2.16.18

//This program generates a random number from 1 to 52 which corresponds to one card in a 52-card deck
//The program then prints out which card the generator selected

import java.lang.Math;

public class CardGenerator {
  public static void main(String[] args) {
    //declares variables
    int cardNumber;
    String suit;
    String identity;
    cardNumber = (int) (Math.random() * 52 + 1); //generates random number between 1 and 52, inclusive
    //System.out.println(cardNumber); //random number check
    
    int identityRemainder = cardNumber % 13; //gets remainder of dividing random number by 13
    
    //changes identity of card from a number to a word if necessary e.g. 11 -> Jack
    switch(identityRemainder) {
      case 1: identityRemainder = 0;
        identity = "King";
        break;
      case 2: identityRemainder = 1;
        identity = "Ace";
        break;
      case 3: identityRemainder = 11;
        identity = "Jack";
        break;
      case 4: identityRemainder = 12;
        identity = "Queen";
        break;
      default: identity = String.valueOf(identityRemainder);
    }
    //System.out.println(identityRemainder); //remainder check
    //System.out.println(identity); //identity check
    
    //assigns suit of card
    if (cardNumber <= 13) {
      suit = "Diamonds";
    } else if (cardNumber >= 14 && cardNumber <= 26) {
      suit = "Clubs";
    } else if (cardNumber >= 27 && cardNumber <= 39) {
      suit = "Hearts";
    } else {
      suit = "Spades";
    }
    //System.out.println(suit); //suit check
    
    //prints out final output of card
    System.out.println("You picked the " + identity + " of " + suit);

  }
}