//Thomas Macon
//2.2.18
//CSE2 Lab 2

//This program takes time in seconds and number of rotations
//of the front wheel as input and outputs the following:
//Number of minutes for each trip
//Number of counts for each trip
//Distance of each trip in miles
//Distance for two trips combined

public class Cyclometer {
  public static void main(String[] args) {
    //recorded data
    int secsTrip1=480;  
  	int secsTrip2=3220;  
  	int countsTrip1=1561;  
  	int countsTrip2=9037; 
    
    //known constants
    double wheelDiameter = 27.0;
    double pi = 3.14159;
    int feetPerMile = 5280;
    int inchesPerFoot = 12;
    int secondsPerMinute = 60;
    double distanceTrip1, distanceTrip2, totalDistance;
    
    //prints calculated output to console
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had "
                      + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had "
                       + countsTrip2 + " counts.");
    
    
    distanceTrip1=countsTrip1*wheelDiameter*pi;
    // Above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*pi/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;

    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");

  } //end of main method
} //end of class