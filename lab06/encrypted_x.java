//Thomas Macon
//CSE2 Lab06
//3.9.18
//Professor Brian Chen

import java.util.Scanner;
import java.lang.Math;

public class encrypted_x {
  public static void main(String[] args) {
    int input = -1;
    String junk;
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Enter an integer between 0 and 100: ");
    while (input < 0) {
      if (myScanner.hasNextInt() == true ) {
        int testInt = myScanner.nextInt();
        if (testInt > 0 && testInt < 100) {
          input = testInt;
        } else {
          System.out.println("INPUT ERROR -> Please enter an integer between 0 and 100: ");
        }
      } else {
        junk = myScanner.next();
        System.out.println("INPUT ERROR -> Please enter an integer between 0 and 100: ");
      }
    }
    
    //NESTED FOR LOOPS
    for (int i = 1; i <= input; i++) {
      for (int j = 1; j <= input; j++) {
        if ( (j<i && j<(input+1-i)) || (j>i && j>(input+1-i)) ) {
          System.out.print("*");
        } else if ( (j>i && j<(input+1-i)) || (j>(input+1-i) && j<i) ) {
          System.out.print("*");
        } else {
        System.out.print(" ");
        }
      }
      System.out.println();
    }
    
    System.out.println(); //divides printouts from the two different methods

    //FOR LOOP AND DO WHILE LOOP
    for (int i = 1; i <= input; i++) {
      int j = 1;
      do {
        if ( (j<i && j<(input+1-i)) || (j>i && j>(input+1-i)) ) {
          System.out.print("*");
          j++;
        } else if ( (j>i && j<(input+1-i)) || (j>(input+1-i) && j<i) ) {
          System.out.print("*");
          j++;
        } else {
          System.out.print(" ");
          j++;
        }
      } while (j <= input);
      System.out.println();
    }
    
  }
}