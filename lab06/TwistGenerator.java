//Thomas Macon
//CSE2 Lab06
//Professor Brian Chen
//3.2.18

//This program asks the user for a positive integer
//The program then prints out a twist that is as many characters long as the inputed integer

import java.util.Scanner;

public class TwistGenerator {
  public static void main(String[] args) {
    int length = 0;
    String junkWord;
    String firstLinePattern = "\\ /";
    String secondLinePattern = " X ";
    String thirdLinePattern = "/ \\";
      
    Scanner myScanner = new Scanner(System.in);
    
    //gets user input. Will loop back until user enters positive integer
    while (length <= 0) {
      System.out.println("Enter a positive integer: ");  
      if (myScanner.hasNextInt() == true) {
        length = myScanner.nextInt();
      } else {
        junkWord = myScanner.next();
      }
    }
    
    int numberOfPatterns = length / 3; //finds number of times pattern will be repeated
    int remainder = length % 3; //finds how many extra characters will be added to end of each line
    String s1 = firstLinePattern.substring(0, remainder); //gets substring to add to end of each line
    String s2 = secondLinePattern.substring(0, remainder);
    String s3 = thirdLinePattern.substring(0, remainder);
    
    //uses for loops to print output then uses a separate print statement to add the extra characters to the end of each line
    for (int i=0; i < numberOfPatterns; i++) {
      System.out.print(firstLinePattern);
    }
    System.out.print(s1 + "\n");
    for (int i=0; i < numberOfPatterns; i++) {
      System.out.print(secondLinePattern);
    }
    System.out.print(s2 + "\n");
    for (int i=0; i < numberOfPatterns; i++) {
      System.out.print(thirdLinePattern);
    }
    System.out.print(s3 + "\n");  
  }
}