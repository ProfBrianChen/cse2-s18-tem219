//Thomas Macon
//CSE2 hw08
//4.10.18
//Professor Brian Chen

//This program calls a method that creates a one-dimensional array of size 10, fills it with random numbers between 0 and 9, inclusive,
//and prints it to the screen. Then it prompts the user to enter an index of the array to remove, then calls another method that does this, and 
//prints out the updated array. It again asks the user for input, this time for an int to be removed from the original array. If that "target" value to be removed
//occurs more than once in the array, the program removes all instances of that value, and prints out the new array. 
//

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput(); //calls method implemented by me
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);

      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index); //calls method implemented by me
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);

      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target); //calls method implemented by me
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);

      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    } while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){ //this method neatly turns an array into a string which it returns
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  public static int[] randomInput() {
    Random randGenerator = new Random(); //creates Random object to generate random numbers
    int[] array = new int[10]; //initialies array of size 10
    for (int i = 0; i < array.length; i++) { //iterates through array
      int rand = randGenerator.nextInt(10); //gets random number
      array[i] = rand; //sets value of array index to a random number
    }
    return array; //returns a filled array of size 10
  }
  
  public static int[] delete(int[] array, int pos) { //this method deletes a specified index from an array
    if (pos < 0 || pos >= array.length) { //checks that input index is valid. If it isn't the method returns original array
      System.out.println("Index entered is out of range of array. Array that has been inputed will be outputed.");
      return array;
    }
    int[] newArray = new int[array.length - 1]; //initializes new array of size one less than input array 
    for (int i = 0; i < newArray.length; i++) { //iterates through array
      if (i < pos) { //checks if loop has reached the index to remove yet. If not, new array is filled with same value as old array at that index
        newArray[i] = array[i];
      } else if (i >= pos) { //if loop has reached index to remove, new array is filled with value from old array index+1 (to skip undwanted index)
        newArray[i] = array[i+1];
      } 
    }
    return newArray; //returns array with size one less than input array since one of the input array's indexes has been removed
  }
  
  public static int[] remove(int[] array, int target) { //this method removes all instances of some input value in the input array
    int shrinkSize = 0; //initiales variable for how much smaller to make the new array than the input array
    for (int i = 0; i < array.length; i++) {
      if (array[i] == target) { //checks if value at current index of input array is equal to target value to be removed
        shrinkSize++; //if this if statement is true, it adds 1 to shrinkSize
      }
    }
    if (shrinkSize == 0) { //if no instance of target value is found, method returns original array
      System.out.println("Element " + target + " was not found");
      return array; //if nothing needs to be removed from input array, a copy of it is returned
    } else {
      System.out.println("Element " + target + " was found");
    }
        
    int[] newArray = new int[array.length - shrinkSize]; //initializes new array with appropriate size compared to input array after target value is removed
    int indexDifference = 0; //initiales variable for how many values have been removed already and so the difference between input array 
    //index and output array index
    for (int i = 0; i < array.length; i++) { //iterates through input array   
      if (array[i] == target) { //if input array value at this index is equal to target value, it adds to indexDifference to "skip" value
        indexDifference++;
      } else { //if input array value at this index is NOT equal to target value, this adds that value to output array
        newArray[i-indexDifference] = array[i];
      }
    } 
    return newArray; //returns array with all instances of target value removed from input array
  }
  
}
