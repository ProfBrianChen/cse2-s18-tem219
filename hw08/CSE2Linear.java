//Thomas Macon
//CSE2 hw08
//4.8.18
//Professor Brian Chen

//This program prompts the user to enter 15 student midterm grades. The program
//prints an error if any of the following three things happen: the user enters a non-integer, 
//a number greater than 100 or less than zero, or a number that is not greater than
//or equal to the previously entered number. Then the program asks the user to enter
//a grade to search for, and a method is run that prints whether or not the grade
//was found and how many iterations of a binary search method it took. Then the program
//calls a method to scramble the array of grades so it isn't in ascending order. Again, the
//user is prompted to enter a grade to search for, and another method is called that prints
//if the grade was found, and how many iterations it took in a linear search method.

import java.util.Scanner; //imports scanner class to get user input
import java.util.Random; //imports random class to generate random numbers

public class CSE2Linear {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //initializes new scanner for user input
    
    int i = 0; //initializes counter for do-while loop
    int[] grades = new int[15]; //initializes empty 1-dimensional array of size 15

    System.out.println("Enter 15 integers representing student grades, where each integer is equal to or gretaer than the previous integer: ");
    do {
      if (myScanner.hasNextInt() == true) { //checks if user entererd an int
        int temp = myScanner.nextInt(); //initializes temporary variable equal to value entered by user
        if (temp >= 0 && temp <= 100) { //checks if value entered is a valid grade
          if (i >= 1) { //checks if at least one value has been entered already so current value can be compared to previous one
            if (temp >= grades[i-1]) { //checks if value is greater than or equal to previous value
              grades[i] = temp; //all input conditions have been met so value is added to array of grades
              i++; //steps counter
            } else { //prints error for not entering grades in ascending order
              System.out.println("ERROR - Enter an integer greater than or equal to the previous integer");
            }
          } else { //adds first value entered (i=1) to array of grades
            grades[i] = temp;
            i++;
          }
        } else { //prints error for not entering valid grade
          System.out.println("ERROR - Integer must be between 0 and 100, inclusive");
        }
      } else { //prints error for not entering an integer
        System.out.println("ERROR - Enter an integer");
        String junk = myScanner.next(); //clears scanner 
      }
    } while (i < 15); //runs until entire array is filled
    
    System.out.println();
    for (int j = 0; j < 15; j++) { //prints complete array of grades
      System.out.print(grades[j] + " ");
    }
    System.out.println();
    
    System.out.println("Enter a grade to search for: ");
    int gradeToSearch = myScanner.nextInt(); //gets grade that user wants to search for
        
    binarySearch(grades, gradeToSearch); //calls method that runs a binary search of array of grades
    int[] scrambledGrades = scrambleArray(grades); //calls method that randomizes grades in array
    
    System.out.println("Enter a grade to search for: ");
    gradeToSearch = myScanner.nextInt(); //gets grade that user wants to search for 
    linearSearch(scrambledGrades, gradeToSearch); //calls method that runs a linear search of randomized array of grades
    
  } //END OF MAIN METHOD
  
  //performs linear search of array of grades
  public static void linearSearch(int[] grades, int gradeToSearch) {
    int i; //declares for loop counter
    for (i = 0; i < grades.length; i++) {
      if (grades[i] == gradeToSearch) { //checks if grade user wants is at current array index 
        System.out.println(gradeToSearch + " was found in the list with " + (i+1) + " iterations");
        return; //if grade is found, method stops running
      }
    }
    //if the method doesn't reach the return statement above, it prints that the array doesn't contain the value the user wants
    System.out.println(gradeToSearch + " was not found in the list with " + (i+1) + " iterations");
  }
  
  //randomizes grades in array
  public static int[] scrambleArray(int[] array) {
    Random randGenerator = new Random();
    for (int i = 0; i < array.length; i++) {
      int rand = randGenerator.nextInt(array.length); //gets random number
      int valToSwitch = array[i]; //saves value at current array index
      //following two lines switch the values at for loop index and random number index
      array[i] = array[rand]; 
      array[rand] = valToSwitch;
    }
   
    //following for loop prints randomized array
    System.out.println("Scrambled array:");
    for (int i = 0; i<array.length; i++) {
      System.out.print(array[i] + " ");
    }
    System.out.println();
    return array; //returns scrambled array 
  }
  
  //performs binary search of array of grades
  public static void binarySearch(int[] grades, int gradeToSearch) {
    boolean cont = true; //initializes variable used in do-while loop condition
    int low = 0;
    int mid = 7;
    int high = 14; 
    int iterations = 0;
    do {
      if (grades[mid] == gradeToSearch) { //checks if the middle of searchable array section is the grade user wants
        iterations++; //steps counter
        System.out.println(gradeToSearch + " was found in the list with " + iterations + " iterations");
        cont = false; //since value was found, sets while condition so the loop breaks
      } else if (grades[mid] > gradeToSearch) { //checks if middle of searchable array section is higher than grade user wants
        high = mid; //cuts searchable array section in half
        iterations++; //steps counter
      } else if (grades[mid] < gradeToSearch) { //checks if middle of searchable array section is lower than grade user wants
        low = mid; //cuts searchable array section in half
        iterations++; //steps counter
      }
      mid = (high + low) / 2; //sets new middle of searcable array section
      
      if (mid == low && grades[mid] != gradeToSearch) { //checks if searchable array section has been shrunk to smallest possible without finding value
        System.out.println(gradeToSearch + " was not found in the list with " + iterations + " iterations");
        cont = false; //method could not find grade that user wants so sets while condition to break the loop
      }        
    } while (cont == true);
  }
}