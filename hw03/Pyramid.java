//Thomas Macon
//CSE02 hw03b
//2.12.18

import java.util.Scanner;

public class Pyramid {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // creates scanner to get user input throughout the program
    System.out.print("Enter the length of one side of the pyramid base: ");
    double base = myScanner.nextDouble(); // receives user input for the base of the pyramid
    System.out.print("Enter the height of the pyramid (be sure to use the same units as above): ");
    double height = myScanner.nextDouble(); // receives user input for the pyramid's height
    double heightCubed = Math.pow(height, 3);
    
    System.out.printf("%#.2f %#.2f %#.2f\n", base, height, heightCubed);
    
    double volumePyramid = (base * heightCubed) / 3; // calculates volume of pyramid in same units that user used to input
    System.out.printf("Volume of pyramid = %#.2f\n", volumePyramid); // prints value calculated above to console

    
  }
}