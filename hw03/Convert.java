//Thomas Macon
//CSE02 hw03
//2.12.18

import java.util.Scanner;

public class Convert {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // creates scanner to get user input throughout the program
    System.out.print("Enter the affected area in acres: ");
    double acres = myScanner.nextDouble(); // receives user input for acreage
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = myScanner.nextDouble(); // receives user input for inches of rain over the affected area
    
    double rainfallInMiles = rainfall / (12 * 5280); // converts inches to miles
    double acresInMiles = (acres * 43560) / (5280*5280); // converts acres to square miles
    
    double cubicMiles = rainfallInMiles * acresInMiles; // calculates volume in cubic miles
    System.out.printf("%#.8f cubic miles\n", cubicMiles); // prints value calculated above to console
    
    
  }
}