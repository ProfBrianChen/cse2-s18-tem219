//Thomas Macon
//CSE2 hw06
//Professor Brian Chen
//3.5.18

//This program requests course information from the user including
//fields such as course number, class time, professor's name, etc.
//The program ensures that the proper type of data is entered for each
//field by using loops. For example, unless an integer is entered for 
//course number or a string is entered for professor, the program will ask
//again for the proper type of input.

import java.util.Scanner; //imports scanner class to get user input

public class Hw06 {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    
    //initializes all values
    int courseNumber = 0;
    String departmentName = "";
    int numberOfMeetings = 0;
    String startingTime = "";
    String instructorName = "";
    int numberStudents = 0;  
    
    //prompts user for department name
    System.out.println("Enter the department name: ");
    if (myScanner.hasNextInt() == false) { //if user doesn't enter an integer, the scanner takes the next scanner object as the department name
      departmentName = myScanner.next();
    } else { //if the user does enter an integer, the program displays an error and requests the correct data type
     //this while loop runs so long as the user enters the incorrect data type
      while (myScanner.hasNextInt() == true) {
        String junkWord = myScanner.next(); //clears scanner of invalid input
        System.out.println("USER INPUT ERROR -> please enter a string: ");
        //For each iteration of the loop, the program checks if the user enters a non-integer with this if-statement.
        //If the user does, the value is assigned to departmentName
        if (myScanner.hasNextInt() == false) { 
          departmentName = myScanner.next();
          break;
        }
      }
    }
    
    //prompts user for class starting time
    System.out.println("Enter the class starting time: ");
    if (myScanner.hasNextInt() == false) { //if user doesn't enter an integer, the scanner takes the next scanner object as the start time
      startingTime = myScanner.next();
    } else { //if the user does enter an integer, the program displays an error and requests the correct data type
      //this while loop runs so long as the user enters the incorrect data type
      while (myScanner.hasNextInt() == true) {
        String junkWord = myScanner.next(); //clears scanner of invalid user input
        System.out.println("USER INPUT ERROR -> please enter a string: ");
        //For each iteration of the loop, the program checks if the user enters a non-integer with this if-statement.
        //If the user does, the value is assigned to startingTime
        if (myScanner.hasNextInt() == false) {
          startingTime = myScanner.next();
          break;
        }
      }
    }
    
    //prompts user for class instructor's name
    System.out.println("Enter the instructor's name: ");
    if (myScanner.hasNextInt() == false) { //if user doesn't enter an integer, the scanner takes the next scanner object as the instructor name
      instructorName = myScanner.next();
    } else { //if the user does enter an integer, the program displays an error and requests the correct data type
      //this while loop runs so long as the user enters the incorrect data type
      while (myScanner.hasNextInt() == true) {
        String junkWord = myScanner.next(); //clears scanner of invalid input
        System.out.println("USER INPUT ERROR -> please enter a string: ");
        //For each iteration of the loop, the program checks if the user enters a non-integer with this if-statement.
        //If the user does, the value is assigned to instructorName
        if (myScanner.hasNextInt() == false) {
          instructorName = myScanner.next();
          break;
        }
      }
    }

    //prompts user for course number
    System.out.println("Enter the course number: "); //should be int    
    //if user enters an integer, program assigns that value to course number
    if (myScanner.hasNextInt() == true) {
      courseNumber = myScanner.nextInt();
    } else { //if the user doesn't enter an integer, program displays error and requests correct data type
      //this while loop runs so long as the user enters the incorrect data type
      while (myScanner.hasNextInt() == false) {
        String junkWord = myScanner.next(); //clears scanner of invalid input
        System.out.println("USER INPUT ERROR -> please enter an integer: ");
        //For each iteration of the loop, the program checks if the user enters an integer with this if-statement.
        //If the user does, the value is assigned to courseNumber
        if (myScanner.hasNextInt() == true) {
          courseNumber = myScanner.nextInt();
          break;
        }
      }
    }

    //prompts user for number of class meetings each week
    System.out.println("Enter the number of meetings per week: "); //should be int  
    //if user enters an integer, program assigns that value to number of meetings
    if (myScanner.hasNextInt() == true) {
      numberOfMeetings = myScanner.nextInt();
    } else { //if the user doesn't enter an integer, program displays error and requests correct data type
      //this while loop runs so long as the user enters the incorrect data type
      while (myScanner.hasNextInt() == false) {
        String junkWord = myScanner.next(); //clears scanner of invalid input
        System.out.println("USER INPUT ERROR -> please enter an integer: ");
        //For each iteration of the loop, the program checks if the user enters an integer with this if-statement.
        //If the user does, the value is assigned to numberOfMeetings
        if (myScanner.hasNextInt() == true) {
          numberOfMeetings = myScanner.nextInt();
          break;
        }
      }
    }
    
    //prompts user for number of students in class
    System.out.println("Enter the number of students in the class: "); //should be int    
    //if user enters an integer, program assigns that value to number of students
    if (myScanner.hasNextInt() == true) {
      numberStudents = myScanner.nextInt();
    } else { //if the user doesn't enter an integer, program displays error and requests correct data type
      //this while loop runs so long as the user enters the incorrect data type
      while (myScanner.hasNextInt() == false) {
        String junkWord = myScanner.next();
        System.out.println("USER INPUT ERROR -> please enter an integer: ");
        //For each iteration of the loop, the program checks if the user enters an integer with this if-statement.
        //If the user does, the value is assigned to numberStudents
        if (myScanner.hasNextInt() == true) {
          numberStudents = myScanner.nextInt();
          break;
        }
      }
    }
  }
}