//Thomas Macon
//CSE2 hw04
//Professor Brian Chen
//2.19.18

//This program performs a random roll of five dice in order to
//play the game Yahtzee. It also gives the user the option to enter 
//a specific roll result by entering a 5-digit number, each digit 
//representing one die. If an invalid 5-digit number is entered, the
//program terminates and displays an error. For each roll, the program will
//calculate the score of each possible scoring method (e.g. twos, 4 of a kind, etc)
//and add them together to get a grand total score.

import java.lang.Math; //imported so program can use Math.random
import java.util.Scanner; //imported so program can use scanner class to get user input
import java.util.Arrays; //imported so program can use Arrays.sort

public class Yahtzee {
  public static void main(String[] args) {
    String userInputRoll; //initializes user input as string
    String rollString = ""; //initializes string to contain random roll values as string
    //initializes an integer for each die value
    int die1;
    int die2;
    int die3;
    int die4;
    int die5;
    
    //initializes an integer for how many of each number are rolled
    int numOnes = 0;
    int numTwos = 0;
    int numThrees = 0;
    int numFours = 0;
    int numFives = 0;
    int numSixes = 0;
    
    //initializes an integer for each individual score
    int aceScore = 0;
    int twoScore = 0;
    int threeScore = 0;
    int fourScore = 0;
    int fiveScore = 0;
    int sixScore = 0;
    int threeOfAKind = 0;
    int fourOfAKind = 0;
    int fullHouse = 0;
    int smallStraight = 0;
    int largeStraight = 0;
    int yahtzee = 0;
    int chance = 0;
    
    //initializes integers for each section score
    int upperScore;
    int upperScoreWithBonus;
    int lowerScore;
    int grandTotal;
   
    Scanner myScanner = new Scanner(System.in); //creates scanner to get user input
    //prompts user for data or to continue with random roll
    System.out.println("Enter a 5-digit number representing a specific 5-dice roll. Or simply press enter to generate a random roll: "); 
    userInputRoll = myScanner.nextLine(); //gets user input as a string

    //if user doesn't enter a roll, program generates random numbers for each die
    if (userInputRoll.equals("")) {
      die1 = (int) (Math.random() * 6 + 1);
      die2 = (int) (Math.random() * 6 + 1);
      die3 = (int) (Math.random() * 6 + 1);
      die4 = (int) (Math.random() * 6 + 1);
      die5 = (int) (Math.random() * 6 + 1);
    } else { 
      //This code is run if the user enters a specific roll
      //It first checks that the user input is valid, then 
      //it takes each character from the input string and converts it to an integer
      if (userInputRoll.length() != 5 || userInputRoll.contains("0") || userInputRoll.contains("7") || userInputRoll.contains("8") 
          || userInputRoll.contains("9")) {
        System.out.println("USER INPUT ERROR: please enter a five-digit number with values between 1 and 6 inclusive");
        System.exit(0);
      }
      die1 = Character.getNumericValue(userInputRoll.charAt(0));
      die2 = Character.getNumericValue(userInputRoll.charAt(1));
      die3 = Character.getNumericValue(userInputRoll.charAt(2));
      die4 = Character.getNumericValue(userInputRoll.charAt(3));
      die5 = Character.getNumericValue(userInputRoll.charAt(4));    
    }
    
    int[] dice = {die1, die2, die3, die4, die5}; //creates array of die values
    Arrays.sort(dice); //sorts dice array in ascending order
    
    //iterates over loop and computes scores for each part of the upper section
    //this loop also finds how many of each number were rolled (fills variables numOnes, numTwos, etc )
    for (int i=0; i < dice.length; i++) {
      rollString += Integer.toString(dice[i]);
      switch (dice[i]) {
        case 1: aceScore += 1;
          numOnes += 1;
          break;
        case 2: twoScore += 2;
          numTwos += 1;
          break;
        case 3: threeScore += 3;
          numThrees += 1;
          break;
        case 4: fourScore += 4;
          numFours += 1;
          break;
        case 5: fiveScore += 5;
          numFives += 1;
          break;
        case 6: sixScore += 6;
          numSixes += 1;
          break;  
      }
    }
    
    //computes upper section score
    upperScore = aceScore + twoScore + threeScore + fourScore + fiveScore + sixScore;
    //if the upper score is greater than 63 this will add the 35 point bonus.
    //However, the way this hw assignment is set up it is impossible to get the
    //bonus because we only score one roll. The upper section will just 
    //be the sum of the dice values
    if (upperScore > 63) {
      upperScoreWithBonus = upperScore + 35;
    } else {
      upperScoreWithBonus = upperScore;
    }
    
    //creates array of how many of each value are in the roll
    int[] numXval = {numOnes, numTwos, numThrees, numFours, numFives, numSixes};
    
    //These booleans will be used in the following for loop
    boolean fullHousePossible1 = false; //initializes boolean to check for full house
    boolean fullHousePossible2 = false; //initializes a second boolean for scoring a full house
    
    //iterates over numXval array to see if there are 3, 4, or 5 of one value
    //If there is, it assigns the appropriate score.
    //It multiplies 3 or 4 by (i+1) because the 0 index of the array corresponds
    //to the number of ones, the 1 index of the array corresponds to the number of 
    //twos, etc
    for (int i=0; i < numXval.length; i++) {
      if (numXval[i] == 2) {
        fullHousePossible1 = true; //since there are two of a kind in the roll, a full house is possible
      } else if (numXval[i] == 3) {
        threeOfAKind = 3 * (i+1);
        fullHousePossible2 = true; //since there are three of a kind in the roll, a full house is possible
      } else if (numXval[i] == 4) {
        fourOfAKind = 4 * (i+1);
      } else if (numXval[i] == 5) {
        yahtzee = 50;
      }
      if (fullHousePossible1 == true && fullHousePossible2 == true) {
        fullHouse = 25; //this if statement scores a full house if the roll contains both a two of a kind and a three of a kind
        threeOfAKind = 0; //cancels out three of a kind score
      }
    }
    
    //this if-else block checks if the roll is a SMALL straight
    if (rollString.contains("1") && rollString.contains("2") && rollString.contains("3") && rollString.contains("4")) {
      smallStraight = 30;
    } else if (rollString.contains("2") && rollString.contains("3") && rollString.contains("4") && rollString.contains("5")) {
      smallStraight = 30;
    } else if (rollString.contains("3") && rollString.contains("4") && rollString.contains("5") && rollString.contains("6")) {
      smallStraight = 30;
    }
    
    //this if-else block checks if the roll is a LARGE straight
    if (rollString.contains("1") && rollString.contains("2") && rollString.contains("3") && rollString.contains("4") && rollString.contains("5")) {
      smallStraight = 0; //cancels out small straight score
      largeStraight = 40; //gives large straight score
    } else if (rollString.contains("2") && rollString.contains("3") && rollString.contains("4") && rollString.contains("5") && rollString.contains("6")) {
      smallStraight = 0; //cancels out small straight score
      largeStraight = 40; //gives large straight score
    }
    
    chance = die1 + die2 + die3 + die4 + die5; //calculates score of chance

    lowerScore = threeOfAKind + fourOfAKind + fullHouse + smallStraight + largeStraight + yahtzee + chance; //calculates lower section score
    
    grandTotal = lowerScore + upperScoreWithBonus; //adds upper and lower section scores to get a total score for the roll
    
    //Prints scores to console
    System.out.println("Upper section initial total: " + upperScore);
    System.out.println("Upper section total with possible bonus: " + upperScoreWithBonus);
    System.out.println("Lower section total: " + lowerScore);
    System.out.println("Grand total: " + grandTotal);

    }
}