//Thomas Macon
//CSE2 lab10
//4.20.18
//Professor Brian Chen

import java.util.Random;

public class Lab10 {
  public static void main(String[] args) {
    Random randGenerator = new Random();
    int width1 = randGenerator.nextInt(10);
    int height1 = randGenerator.nextInt(10);
    int width2 = randGenerator.nextInt(10);
    int height2 = randGenerator.nextInt(10);
    
    System.out.println(width1);
    System.out.println(height1);
    
    int[][] array = increasingMatrix(width1, height1, true);
    printMatrix(array, true);
  }
  
  public static int[][] increasingMatrix(int width, int height, boolean format) {
    int[][] array = new int[height][width];
    int x = 0;
    int y = 0;
    for (int i = 1; i <= (width*height); i++) {
      System.out.println(x + " " + y);
      array[y][x] = i;
      if (format == true && i < width) {
        x++;
      } else if (format == true && ((i%width) == 0) ) {
        x = 0;
        y++;
      } else if (format == false && i < height) {
        y++;
      } else if (format == false && ((i%height) == 0) ) {
        x++;
        y = 0;
      }
    }
    return array;
  }

  //array.length gives number of rows
  
  public static void printMatrix(int[][] array, boolean format) {
    
    for (int i = 0; i < array.length; i++) {
        System.out.print("[ ");
      for (int j = 0; j < array[i].length; j++) {
        if (format == true) {
          System.out.print(array[i][j] + " ");
        } else {
          System.out.print(array[j][i] + " ");
        }
      }
      System.out.printf("]\n");
    }
  }
  
  
  
}