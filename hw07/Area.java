//Thomas Macon
//CSE2 hw07
//3.17.18
//Professor Brian Chen

//This program gives the user the option to calculate the area of one of three shapes, either a rectangle, triangle, or a circle.
//It then asks the user for the dimensions of that shape in Double format. If the user enters a different shape or doesn't 
//enter Doubles for the dimensions, the program will print an error and ask for valid input.
//Once the program receives valid input, it will calculate the area of whatever shape was selected and print the result.

import java.util.Scanner; //imports scanner class to get user input

public class Area {
  public static void main(String[] args) { //main method 
    Double[] input = getUserInput(); //runs the method getUserInput and assigns its output to a variable to be used in main method
    //the five variables below are from the output of getUserInput
    Double shapeIdentifier = input[0];
    Double height = input[1];
    Double width = input[2];
    Double base = input[3];
    Double radius = input[4];
      
    if (shapeIdentifier == 1.0) { //"rectangle" was entered
      rectangleArea(height, width); //calls method to calculate and print area of a rectangle
    } else if (shapeIdentifier == 2.0) { //"triangle" was entered
      triangleArea(base, height); //calls method to calculate and print area of a triangle
    } else if (shapeIdentifier == 3.0) { //"cirlce" was entered
      circleArea(radius); //calls method to calculate and print area of a cirlce
    }   
  } //END OF MAIN METHOD
  
  public static Double[] getUserInput() {
    Scanner myScanner = new Scanner(System.in); //declares scanner to get user input
    String shape;
    String junkInput;
    boolean shapeInputValid = false; //this will be used to check if string user enters for the shape is valid
    Double shapeIdentifier = 0.0;
    Double height = 0.0;
    Double width = 0.0;
    Double base = 0.0;
    Double radius = 0.0;

    System.out.println("Enter the shape you want to calculate the area of (use all lowercase letters): ");

    do {
      shape = myScanner.next(); //assigns user input to variable
      if (shape.equals("rectangle")) {
        shapeInputValid = true; //this will break the while loop condition and exit the input loop
        shapeIdentifier = 1.0; //this will be returned at the end of this method so it can be used in another method to see what shape was entered
        
        System.out.println("Enter the rectangle's height: ");
        while (height <= 0 ) { //this loop will always run at least once because this variable was declared as 0.0
          if (myScanner.hasNextDouble()) { //checks if user entered a double
            height = myScanner.nextDouble(); //assigns user input to variable
            if (height <= 0) { //checks that number that user entered is positive
              System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
            }
          } else {
            junkInput = myScanner.next(); //if input is invalid 
            System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
          }
        }
        
        System.out.println("Enter the rectangle's width: ");
        while (width <= 0 ) { //this loop will always run at least once because this variable was declared as 0.0
          if (myScanner.hasNextDouble()) { //checks if user entered a double
            width = myScanner.nextDouble(); //assigns user input to variable
            if (width <= 0) {
              System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
            }
          } else {
            junkInput = myScanner.next(); //if the input is invalid, this assignment clears the scanner so it can be used again
            System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
          }
        }

      } else if (shape.equals("triangle")) {
        shapeInputValid = true; //this will break the while loop condition and exit the input loop
        shapeIdentifier = 2.0; //this will be returned at the end of this method so it can be used in another method to see what shape was entered

        System.out.println("Enter the triangle's base: ");
        while (base <= 0 ) { //this loop will always run at least once because this variable was declared as 0.0
          if (myScanner.hasNextDouble()) { //checks if user entered a double
            base = myScanner.nextDouble(); //assigns user input to variable
            if (base <= 0) {
              System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
            }
          } else {
            junkInput = myScanner.next(); //if the input is invalid, this assignment clears the scanner so it can be used again
            System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
          }
        }
        
        System.out.println("Enter the triangle's height: ");
        while (height <= 0 ) { //this loop will always run at least once because this variable was declared as 0.0
          if (myScanner.hasNextDouble()) { //checks if user entered a double
            height = myScanner.nextDouble(); //assigns user input to variable
            if (height <= 0) {
              System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
            }
          } else {
            junkInput = myScanner.next(); //if the input is invalid, this assignment clears the scanner so it can be used again
            System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
          }
        }
        
      } else if (shape.equals("circle")) {
        shapeInputValid = true; //this will break the while loop condition and exit the input loop
        shapeIdentifier = 3.0; //this will be returned at the end of this method so it can be used in another method to see what shape was entered
        
        System.out.println("Enter the circle's radius: ");
        while (radius <= 0 ) { //this loop will always run at least once because this variable was declared as 0.0
          if (myScanner.hasNextDouble()) {
            radius = myScanner.nextDouble(); //assigns user input to variable
            if (radius <= 0) {
              System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
            }
          } else {
            junkInput = myScanner.next(); //if the input is invalid, this assignment clears the scanner so it can be used again
            System.out.println("INPUT ERROR -> Please enter a positive double: "); //if input is invalid an error message is printed
          }
        }

      } else {
        System.out.println("USER INPUT ERROR -> please enter either 'rectangle,' 'triangle,' or 'circle: '"); //if input is invalid an error message is printed    
      }
    } while (shapeInputValid == false); //this loop will continue to run until the user enteres a valid shape
      
    Double[] output = {shapeIdentifier, height, width, base, radius}; //puts all variables that may have been inputed into an array.
    //Based on which shape the user entered, some of the values in the array above will be 0.0 since that's what they were declared
    //as and they weren't changed during this method. This is ok because the variables required for the particular shape 
    //will be non-zero and will be the only variables used later in the program. 
    return output; //returns the above array
  }
  
  public static void rectangleArea(Double height, Double width) { //calculates the area of a rectangle with the two input dimensions
    Double area = height * width; //calculates area
    System.out.printf("The area of the rectangle = %5.2f\n", area); //prints result with two decimal places
  }
  
  public static void triangleArea(Double base, Double height) { //calculates the area of a triangle with the two input dimensions
    Double area = 0.5 * base *height; //calculates area
    System.out.printf("The area of the triangle = %5.2f\n", area); //prints result with two decimal places
  }
  
  public static void circleArea(Double radius) { //calculates the area of a circle with the input radius
    Double area = Math.PI * (radius * radius); //calculates area
    System.out.printf("The area of the cirlce = %5.2f\n", area); //prints result with two decimal places
  }
  
} //END OF CLASS