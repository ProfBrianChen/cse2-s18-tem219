//Thomas Macon
//CSE2 hw06
//3.20.18
//Professor Brian Chen

//This program displays an argyle pattern based on user input for the dimensions of the display area, the size of the diamonds,
//the width of the x-shaped stripes, and the characters used for the diamonds, background, and stripes. It ensures that all of 
//the dimensions entered are positive integers and that the stripe width is an odd number no larger than half the diamond size.the

import java.util.Scanner; //imports scanner class to get user input

public class Argyle {
  public static void main(String[] args) {
    //initializes variables
    //the integers are initialized as -1 because a while loop with the condition of being less than zero is used
    //until a valid number is entered. 
    int width = -1;
    int height = -1;
    int diamondSize = -1;
    int stripeWidth = -1;
    char diamondChar;
    char backgroundChar;
    char stripeChar;
    String junkInput;
    String temp;
    
    Scanner myScanner = new Scanner(System.in); //creates a new scanner to use throughout program
    
    System.out.print("Enter the width of the display window: ");
    while (width < 0) { //always initially true because all integers are initialized as -1; so this loop always runs at least once
      if (myScanner.hasNextInt() == true ) { //checks if the user entered an integer
        int testInt = myScanner.nextInt();
        if (testInt > 0) { //if an integer is entered this checks that it is positive
          width = testInt; //if the input is satisfactory, it is assigned to the variable
        } else {
          System.out.print("INPUT ERROR -> Please enter a positive integer: "); //if the input is not satisfactory, the program asks the user for another value
        } 
      }
      else {
        junkInput = myScanner.next(); //if the input is not satisfactory, it is assigned to a "junk" variable in order to clear the scanner
        System.out.print("INPUT ERROR -> Please enter a positive integer: "); //if the input is not satisfactory, the program asks the user for another value
      }
    }
    
    //*****************************************************
    //ALL THE COMMENTS FOR THE ABOVE WHILE LOOP APPLY TO ALL OF THE FOLLOWING WHILE LOOPS
    //*****************************************************
    
    System.out.print("Enter the height of the display window: ");
    while (height < 0) {
      if (myScanner.hasNextInt() == true ) {
        int testInt = myScanner.nextInt();
        if (testInt > 0) {
          height = testInt;
        } else {
          System.out.print("INPUT ERROR -> Please enter a positive integer: ");
        } 
      }
      else {
        junkInput = myScanner.next();
        System.out.print("INPUT ERROR -> Please enter a positive integer: ");
      }
    }
    
    System.out.print("Enter the size of the argyle diamonds: ");
    while (diamondSize < 0) {
      if (myScanner.hasNextInt() == true ) {
        int testInt = myScanner.nextInt();
        if (testInt > 0) {
          diamondSize = testInt;
        } else {
          System.out.print("INPUT ERROR -> Please enter a positive integer: ");
        } 
      }
      else {
        junkInput = myScanner.next();
        System.out.print("INPUT ERROR -> Please enter a positive integer: ");
      }
    }
    
    System.out.print("Enter the width of the argyle stripe: ");
    while (stripeWidth < 0) {
      if (myScanner.hasNextInt() == true ) {
        int testInt = myScanner.nextInt();
        if (testInt > 0 && testInt%2 != 0 && testInt <= diamondSize/2) {
          stripeWidth = testInt;
        } else {
          System.out.print("INPUT ERROR -> Please enter a positive, odd integer no larger than half the diamond size: ");
        } 
      }
      else {
        junkInput = myScanner.next();
        System.out.print("INPUT ERROR -> Please enter a positive, odd integer no larger than half the diamond size: ");
      }
    }
    
    //The following three blocks get user input for characters to be used in the pattern
    System.out.print("Enter a character to fill the argyle diamonds: ");
    temp = myScanner.next();
    diamondChar = temp.charAt(0);
    
    System.out.print("Enter a character for the background: ");
    temp = myScanner.next();
    backgroundChar = temp.charAt(0);
    
    System.out.print("Enter a character to fill the stripe: ");
    temp = myScanner.next();
    stripeChar = temp.charAt(0);   
    
    
    //The first for loop below iterates through each line of the pattern (vertical position).
    //The second for loop iterates through each character on each line (horizontal position).
    //Inside the nested for loops there are four main if/if else statements that create each quadrant of
    //a single diamond. The quadrants labeled follow the quadrants of an x-y axis: for example, the first 
    //quadrant is positive x and positive y (upper right), the second quadrant is negative x and 
    //positive y (upper left).
    
    
    for (int i = 0; i < height; i++) {
      int yIndex = (i % (2*diamondSize)) + 1; //creates a variable that gives the vertical position inside a single DIAMOND
      int ySubIndex = (i % diamondSize) + 1; //creates a variable that gives the vertical position inside a single QUADRANT
      for (int j = 0; j < width; j++) {
        int xIndex = (j % (2*diamondSize)) + 1; //creates a variable that gives the horizontal position inside a single DIAMOND
        int xSubIndex = (j % diamondSize) + 1; //creates a variable that gives the horizontal position inside a single QUADRANT
        if ( (xIndex <= diamondSize) && (yIndex <= diamondSize) ) { //checks if position is in the second quadrant of a diamond
          //SECOND QUADRANT
          if (ySubIndex >= xSubIndex-(stripeWidth/2) && ySubIndex <= xSubIndex+(stripeWidth/2)) {
            System.out.print(stripeChar); //if the position corresponds to a stripe position, it prints the character for stripes
          } else if (diamondSize-xSubIndex+1 < ySubIndex) { //if the position corresponds to a diamond position, it prints the diamond character
            System.out.print(diamondChar);
          } else { 
            System.out.print(backgroundChar); //if the position is neither a stripe nor a diamond position, it prints the character for the background
          }
        } 
        else if ( (xIndex > diamondSize) && (yIndex <= diamondSize) ) { //checks if position is in the first quadrant of a diamond
          //FIRST QUADRANT
          if (ySubIndex >= diamondSize+1-xSubIndex-(stripeWidth/2) && ySubIndex <= diamondSize+1-xSubIndex+(stripeWidth/2)) {
            System.out.print(stripeChar); //if the position corresponds to a stripe position, it prints the character for stripes
          } else if (ySubIndex > xSubIndex) {
            System.out.print(diamondChar); //if the position corresponds to a diamond position, it prints the diamond character
          } else {
            System.out.print(backgroundChar); //if the position is neither a stripe nor a diamond position, it prints the character for the background
          }
        } 
        else if ( (xIndex <= diamondSize) && (yIndex > diamondSize) ) { //checks if position is in the third quadrant of a diamond
          //THIRD QUADRANT
          if (ySubIndex >= diamondSize+1-xSubIndex-(stripeWidth/2) && ySubIndex <= diamondSize+1-xSubIndex+(stripeWidth/2)) {
            System.out.print(stripeChar); //if the position corresponds to a stripe position, it prints the character for stripes
          } else if (ySubIndex <= xSubIndex) {
            System.out.print(diamondChar); //if the position corresponds to a diamond position, it prints the diamond character
          } else {
            System.out.print(backgroundChar); //if the position is neither a stripe nor a diamond position, it prints the character for the background
          }
        } 
        else if ( (xIndex > diamondSize) && (yIndex > diamondSize) ) { //checks if position is in the fourth quadrant of a diamond
          //FOURTH QUADRANT
          if (ySubIndex >= xSubIndex-(stripeWidth/2) && ySubIndex <= xSubIndex+(stripeWidth/2)) {
            System.out.print(stripeChar); //if the position corresponds to a stripe position, it prints the character for stripes
          } else if (diamondSize-xSubIndex+1 >= ySubIndex) {
            System.out.print(diamondChar); //if the position corresponds to a diamond position, it prints the diamond character
          } else {
            System.out.print(backgroundChar); //if the position is neither a stripe nor a diamond position, it prints the character for the background
          }
        }
      }
      System.out.println(""); //after an entire line is printed, this statement makes the program go to the next line
    }
    
    
    //********************************************
    //THE FOLLOWING BLOCKS ARE FOR CREATING THE FOUR DIFFERENT QUADRANTS OF A SINGLE DIAMOND PATTERN
    //THEY WERE USED AS A BASE TO WRITE THE FULL PROGRAM ABOVE
    //*********************************************

//     //SECOND QUADRANT
//     for (int i = 1; i <= diamondSize; i++) {
//       for (int j = 1; j <= diamondSize; j++) {
//         if (i >= j-(stripeWidth/2) && i <= j+(stripeWidth/2)) {
//           System.out.print(stripeChar);
//         } else if (diamondSize-j+1 < i) {
//           System.out.print(diamondChar);
//         } else {
//           System.out.print(backgroundChar);
//         }
//       }
//       System.out.println("");
//     }
    
//     System.out.println("");
    
//     //FOURTH QUADRANT
//     for (int i = 1; i <= diamondSize; i++) {
//       for (int j = 1; j <= diamondSize; j++) {
//         if (i >= j-(stripeWidth/2) && i <= j+(stripeWidth/2)) {
//           System.out.print(stripeChar);
//         } else if (diamondSize-j+1 >= i) {
//           System.out.print(diamondChar);
//         } else {
//           System.out.print(backgroundChar);
//         }
//       }
//       System.out.println("");
//     }
    
//     System.out.println("");
    
//     //FIRST QUADRANT
//     for (int i = 1; i <= diamondSize; i++) {
//       for (int j = 1; j <= diamondSize; j++) {
//         if (i >= diamondSize+1-j-(stripeWidth/2) && i <= diamondSize+1-j+(stripeWidth/2)) {
//           System.out.print(stripeChar);
//         } else if (i > j) {
//           System.out.print(diamondChar);
//         } else {
//           System.out.print(backgroundChar);
//         }
//       }
//       System.out.println("");
//     }
    
//     System.out.println("");
    
//     //THIRD QUADRANT
//     for (int i = 1; i <= diamondSize; i++) {
//       for (int j = 1; j <= diamondSize; j++) {
//         if (i >= diamondSize+1-j-(stripeWidth/2) && i <= diamondSize+1-j+(stripeWidth/2)) {
//           System.out.print(stripeChar);
//         } else if (i <= j) {
//           System.out.print(diamondChar);
//         } else {
//           System.out.print(backgroundChar);
//         }
//       }
//       System.out.println("");
//     }
    
    
  }
}