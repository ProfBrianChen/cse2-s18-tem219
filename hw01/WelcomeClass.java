public class WelcomeClass {
  public static void main(String[] args) {
    System.out.println("  -----------\n  | WELCOME | \n  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^  \n / \\/ \\/ \\/ \\/ \\/ \\ \n<-T--E--M--2--1--9->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / \n  v  v  v  v  v  v");
  }
}
