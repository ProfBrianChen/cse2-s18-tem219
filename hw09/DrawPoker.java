//Thomas Macon
//CSE2 hw09
//4.17.18
//Professor Brian Chen

//This program deals a five-card poker hand to two "players," calls methods to see if various hands are held (pair, flush, full house, etc),
//and determines the winner. If nobody has a typical poker hand, the player with the highest card wins. To simplify the program, if both 
//players have the same category of hand (such as a pair), the game is considered a tie, even though in a real game a higher pair would win. 

import java.util.Random;

public class DrawPoker {
  public static void main(String[] args) {
<<<<<<< HEAD
    int[] deck = new int[52];
    int[] hand1 = new int[5];
    int[] hand2 = new int[5];
    int hand1Score;
    int hand2Score;

    Random randGenerator = new Random();
=======
    int[] deck = new int[52]; //initializes deck of 52 card
    int[] hand1 = new int[5]; //initializes hand of 5 cards
    int[] hand2 = new int[5]; //initializes another hand of 5 cards
    int hand1Score;
    int hand2Score;

    Random randGenerator = new Random(); //for getting random numbers
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
    
    for(int i = 0; i < deck.length; i++) { //populates array deck with numbers from 0 to 52, inclusive
      deck[i] = i;
    }

    for (int i = 0; i < deck.length; i++) { //this loop shuffles the deck
      int rand = randGenerator.nextInt(deck.length); //gets random number
      int valToSwitch = deck[i]; //saves value at current array index
      //following two lines switch the values at for loop index and random number index
      deck[i] = deck[rand]; 
      deck[rand] = valToSwitch;
    }
    
    int j = 0; //hand1 index
    int k = 0; //hand2 index
<<<<<<< HEAD
=======
    //following loop deals 5 cards to each hand, alternating between hands with each new card
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
    for(int i = 0; i < 10; i++) {
      if (i%2 == 0) {
        hand1[j] = deck[i];
        j++;
      } else {
        hand2[k] = deck[i];
        k++;
      }
    }

<<<<<<< HEAD
    displayHands(hand1, hand2);
    
    if (pair(hand1)) {
=======
    displayHands(hand1, hand2); //calls method to print out each hand with easy-to-read formatting
    
    //the following two if/elseif blocks are the same, one block for each hand. They call methods to check if the hand
    //is a pair, three of a kind, flush, or full house. If it it none of these, then the highest card in the hand is taken
    //as the score. The other scores for each hand (e.g. 40 for a flush) are arbitrary except that they are ordered in the 
    //proper manner for regular poker scoring, e.g. a flush (40 points) beats a three of a kind (30 points).
    if (pair(hand1)) { 
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
      hand1Score = 20;
    } else if (threeOfAKind(hand1)) {
      hand1Score = 30;
    } else if (flush(hand1)) {
      hand1Score = 40;
    } else if (fullHouse(hand1)) {
      hand1Score = 50;
    } else {
      hand1Score = highCard(hand1);
    }
    
    if (pair(hand2)) {
      hand2Score = 20;
    } else if (threeOfAKind(hand2)) {
      hand2Score = 30;
    } else if (flush(hand2)) {
      hand2Score = 40;
    } else if (fullHouse(hand2)) {
      hand2Score = 50;
    } else {
      hand2Score = highCard(hand2);
    }
    
<<<<<<< HEAD
=======
    //following block compares scores and prints out who won the game, or if it was a tie
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
    if (hand1Score > hand2Score) {
      System.out.println("Player 1 wins!");
    } else if (hand2Score > hand1Score) {
      System.out.println("Player 2 wins!");
    } else {
      System.out.println("Game is a tie.");
    }
<<<<<<< HEAD
    
    
    
    System.out.println(pair(hand1));
    System.out.println(pair(hand2));
    System.out.println(threeOfAKind(hand1));
    System.out.println(threeOfAKind(hand2));
    System.out.println(fullHouse(hand1));
    System.out.println(fullHouse(hand2));
    System.out.println(flush(hand1));
    System.out.println(flush(hand2));
    
 
  }
  
  public static int[] handValues(int[] hand) {  
    int[] handVals = new int[5];
    for (int i=0; i < hand.length; i++) {
      handVals[i] = (hand[i] % 13) + 1; 
      System.out.println(handVals[i]);
    } 
    return handVals;
  }
  
  public static String[] handSuits(int[] hand) {
    String[] handSuits = new String[5];
    for (int i=0; i < hand.length; i++) {
=======
  } //END OF MAIN METHOD
  
  //following method takes a hand array as input and returns an array with the numerical value for each card in the hand
  public static int[] handValues(int[] hand) {  
    int[] handVals = new int[5];
    for (int i=0; i < hand.length; i++) { //iterates through hand and finds the numerical value of each card
      handVals[i] = (hand[i] % 13) + 1; 
    } 
    return handVals;
  } //END OF handValues
  
  //following method takes a hand array as input and returns an array with the suit of each card in the hand
  public static String[] handSuits(int[] hand) {
    String[] handSuits = new String[5];
    for (int i=0; i < hand.length; i++) { //iterates though hand and finds suit of each card
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
      if (hand[i] <= 12) {
        handSuits[i] = "diamonds";
      } else if (hand[i] >= 13 && hand[i] <= 25) {
        handSuits[i] = "clubs";
      } else if (hand[i] >= 26 && hand[i] <= 38) {
        handSuits[i] = "hearts";
      } else {
        handSuits[i] = "spades";
      }
<<<<<<< HEAD
      System.out.println(handSuits[i]);
    }
    return handSuits;
  }
  
  public static void displayHands(int[] hand1, int[] hand2) {
    int[] hand1Vals = handValues(hand1);
    String[] hand1Suits = handSuits(hand1);
    int[] hand2Vals = handValues(hand2);
    String[] hand2Suits = handSuits(hand2);
=======
    }
    return handSuits;
  } //END OF handSuits
  
  //following method prints out each hand in a formatted, easy-to-read way
  public static void displayHands(int[] hand1, int[] hand2) {
    int[] hand1Vals = handValues(hand1); //gets array of numberical values inn hand
    String[] hand1Suits = handSuits(hand1); //gets array of suits in hand
    int[] hand2Vals = handValues(hand2); //gets array of numberical values inn hand
    String[] hand2Suits = handSuits(hand2); //gets array of suits in hand
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
    
    System.out.printf("Hand 1:\n{");
    for (int i = 0; i < hand1.length; i++) {
      System.out.print(hand1Vals[i] + " of " + hand1Suits[i] + ", ");
    }
    System.out.printf("}\n");
    System.out.printf("Hand 2:\n{");
    for (int i = 0; i < hand2.length; i++) {
      System.out.print(hand2Vals[i] + " of " + hand2Suits[i] + ", ");
    }
    System.out.printf("}\n");
<<<<<<< HEAD
  }
  
  public static int[] numberOfEachValue(int[] hand) { //returns an array of how many of each numerical value are in the hand
    int[] numberOfEachVal = new int[13];
    for (int i = 0; i < hand.length; i++) {
      int val = hand[i] % 13;
      switch (val) {
=======
  } //END OF displayHands
  
  //following method finds how many of each numerical value are in a hand. This method is used by other methods to see if 
  //a hand is a pair, three of a kind, or full house.
  public static int[] numberOfEachValue(int[] hand) { 
    int[] numberOfEachVal = new int[13];
    for (int i = 0; i < hand.length; i++) {
      int val = hand[i] % 13;
      switch (val) { //has a case for each numerical value (ace through king) and adds to an array to show how many of each are in the hand
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
        case 0: numberOfEachVal[0] += 1;
          break;
        case 1: numberOfEachVal[1] += 1;
          break;
        case 2: numberOfEachVal[2] += 1;
          break;
        case 3: numberOfEachVal[3] += 1;
          break;
        case 4: numberOfEachVal[4] += 1;
          break;
        case 5: numberOfEachVal[5] += 1;
          break;  
        case 6: numberOfEachVal[6] += 1;
          break;
        case 7: numberOfEachVal[7] += 1;
          break;
        case 8: numberOfEachVal[8] += 1;
          break;
        case 9: numberOfEachVal[9] += 1;
          break;
        case 10: numberOfEachVal[10] += 1;
          break;
        case 11: numberOfEachVal[11] += 1;
          break; 
        case 12: numberOfEachVal[12] += 1;
      }
    }
    return numberOfEachVal;
<<<<<<< HEAD
  }
  
  public static int highCard(int[] hand) {
    int[] handVals = handValues(hand);
    int highCard = 0;
    for (int i = 0; i < hand.length; i++) {
=======
  } //END OF numberOfEachValue
  
  //following method returns the maximum value in a hand 
  public static int highCard(int[] hand) {
    int[] handVals = handValues(hand);
    int highCard = 0;
    for (int i = 0; i < hand.length; i++) { //iterates through hand and sets the value as the high card if it is greater than the previously saved high card
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
      if (handVals[i] > highCard) {
        highCard = handVals[i];
      }
    }
    return highCard;
<<<<<<< HEAD
  }
  
  public static boolean pair(int[] hand) {
    int[] numberOfEachVal = numberOfEachValue(hand);
    for (int i = 0; i < numberOfEachVal.length; i++) {
      if (numberOfEachVal[i] == 2) {
=======
  } //END OF highCard
  
  //following method returns true if a hand has a pair, returns false otherwise
  public static boolean pair(int[] hand) {
    int[] numberOfEachVal = numberOfEachValue(hand); //uses another method to find how many of each value are in the hand
    for (int i = 0; i < numberOfEachVal.length; i++) {
      if (numberOfEachVal[i] == 2) { //if there are two of one value in the hand, returns true
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
        return true;
      }
    }
    return false;
<<<<<<< HEAD
  } 
  
  public static boolean threeOfAKind(int[] hand) {
    int[] numberOfEachVal = numberOfEachValue(hand);    
    for (int i = 0; i < numberOfEachVal.length; i++) {
      if (numberOfEachVal[i] == 3) {
=======
  } //END OF pair
  
  //following method returns true if a hand has a three of a kind, returns false otherwise
  public static boolean threeOfAKind(int[] hand) {
    int[] numberOfEachVal = numberOfEachValue(hand); //uses another method to find how many of each value are in the hand
    for (int i = 0; i < numberOfEachVal.length; i++) { 
      if (numberOfEachVal[i] == 3) { //if there are three of one value in the hand, returns true
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
        return true;
      }
    }
    return false;
<<<<<<< HEAD
  }     
  
  public static boolean fullHouse(int[] hand) {
    boolean fullHousePossible1 = false;
    boolean fullHousePossible2 = false;
    int[] numberOfEachVal = numberOfEachValue(hand);    
    for (int i = 0; i < numberOfEachVal.length; i++) {
      if (numberOfEachVal[i] == 3) {
        fullHousePossible1 = true;
      } else if (numberOfEachVal[i] == 2) {
        fullHousePossible2 = true;
      }
    }
    if (fullHousePossible1 == true && fullHousePossible2 == true) {
=======
  } //END OF threeOfAKind 
  
  //following method returns true if a hand is a full house, returns false otherwise
  public static boolean fullHouse(int[] hand) {
    //following two lines set initial conditions for full house as false. Both of these booleans must be changed to true
    //for the method to return true
    boolean fullHousePossible1 = false; 
    boolean fullHousePossible2 = false; 
    int[] numberOfEachVal = numberOfEachValue(hand); //uses another method to find how many of each value are in the hand
    for (int i = 0; i < numberOfEachVal.length; i++) {
      if (numberOfEachVal[i] == 3) { //sets condition of hand having a three of a kind to true
        fullHousePossible1 = true;
      } else if (numberOfEachVal[i] == 2) { //sets condition of hand hanving a pair to true
        fullHousePossible2 = true;
      }
    }
    if (fullHousePossible1 == true && fullHousePossible2 == true) { //if hand has a three of a kind AND a pair, method returns true
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
      return true;
    } else {
      return false;
    }
<<<<<<< HEAD
  }
  
  public static boolean flush(int[] hand) {
    int[] numberOfEachSuit = new int[4];
    for (int i=0; i < hand.length; i++) {
=======
  } //END OF fullHouse
  
  //following method returns true if a hand is a flush
  public static boolean flush(int[] hand) {
    int[] numberOfEachSuit = new int[4];
    for (int i=0; i < hand.length; i++) { //iterates through hand and finds how many of each suit are in hand
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
      if (hand[i] <= 12) {
        numberOfEachSuit[0] += 1;
      } else if (hand[i] >= 13 && hand[i] <= 25) {
        numberOfEachSuit[1] += 1;
      } else if (hand[i] >= 26 && hand[i] <= 38) {
        numberOfEachSuit[2] += 1;
      } else {
        numberOfEachSuit[3] += 1;
      }
    }  
    for (int i = 0; i < numberOfEachSuit.length; i++) {
<<<<<<< HEAD
      if (numberOfEachSuit[i] == 5) {
=======
      if (numberOfEachSuit[i] == 5) { //if hand has five of any suit, method returns true
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
        return true;
      }
    }
    return false;
<<<<<<< HEAD
  }

}
=======
  } //END OF flush

} //END OF CLASS DrawPoker
>>>>>>> 1878652e0347eee67eb7f73afbeedb02af7e543e
