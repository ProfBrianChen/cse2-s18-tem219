//Thomas Macon
//CSE 2 lab02
//2.9.18

//This code asks the user for the total cost of a meal, the percentage tip they want to pay, and
//the number of ways they want to split the check.
//Then it calculates how much each person should pay.

import java.util.Scanner;

public class Check {
  public static void main(String[] args) {
    //Gets input from user
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the originial cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //declares variables
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    totalCost = checkCost * (1 + tipPercent); //calculates total bill including tip
    costPerPerson = totalCost / numPeople; //
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    //prints final output
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }
}